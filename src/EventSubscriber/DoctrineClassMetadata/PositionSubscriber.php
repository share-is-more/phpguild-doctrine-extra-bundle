<?php

declare(strict_types=1);

namespace PhpGuild\DoctrineExtraBundle\EventSubscriber\DoctrineClassMetadata;

use Doctrine\Bundle\DoctrineBundle\Attribute\AsDoctrineListener;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Events;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use PhpGuild\DoctrineExtraBundle\Model\Position\PositionInterface;

/**
 * Class PositionSubscriber.
 */
#[AsDoctrineListener(event: Events::onFlush, priority: 256)]
final class PositionSubscriber
{

    /**
     * onFlush
     *
     * @param OnFlushEventArgs $eventArgs
     *
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function onFlush(OnFlushEventArgs $eventArgs): void
    {
        $entityManager = $eventArgs->getEntityManager();
        $unitOfWork = $entityManager->getUnitOfWork();

        $filterCollection = $entityManager->getFilters();
        $enabledFilters = array_keys($filterCollection->getEnabledFilters());

        $nextPosition = null;

        foreach ($unitOfWork->getScheduledEntityInsertions() as $entity) {
            if (!$entity instanceof PositionInterface || null !== $entity->getPosition()) {
                continue;
            }

            $className = \get_class($entity);

            if (null === $nextPosition) {
                foreach ($enabledFilters as $filter) {
                    $filterCollection->disable($filter);
                }

                $nextPosition = $entityManager->getRepository($className)
                    ->createQueryBuilder('e')
                    ->select(sprintf('MAX(e.%s)', PositionInterface::POSITION_FIELD_NAME))
                    ->getQuery()
                    ->getSingleScalarResult();

                foreach ($enabledFilters as $filter) {
                    $filterCollection->enable($filter);
                }
            }

            $nextPosition = null === $nextPosition ? 0 : $nextPosition + 1;
            $entity->setPosition($nextPosition);

            $unitOfWork->recomputeSingleEntityChangeSet($entityManager->getClassMetadata($className), $entity);
        }
    }
}
