<?php

declare(strict_types=1);

namespace PhpGuild\DoctrineExtraBundle\Model\Uuid;

use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Trait UuidPropertiesTrait
 */
trait UuidPropertiesTrait
{
    /** @var string|null $id */
    #[Groups('default')]
    protected $id;
}
